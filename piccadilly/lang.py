#!/usr/bin/env python

# Copyright (c) 2014 Andrei Tatar. All rights reserved.


from piccadilly import errors


class Identifier(object):

    def __init__(self, name):
        self.name = str(name)

    def __str__(self):
        return '/' + self.name

    def __repr__(self):
        return 'piccadilly.lang.Identifier("{!s}")'.format(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return self.name.__hash__()


class Block(object):

    def __init__(self, parent=None):
        self.parent = parent
        self.code = []
        self.namespace = {}

    def __iter__(self):
        for element in self.code:
            yield element

    def __getitem__(self, key):
        return self.code[key]

    def __setitem__(self, key, value):
        self.code[key] = value

    def __str__(self):
        return '{\n' + ' '.join((str(x) for x in self)) + '\n}'

    def inject_names(self, namespace):
        self.namespace.update(namespace)

    def append(self, element):
        self.code.append(element)

    def extend(self, elements):
        self.code.extend(elements)

    def resolve(self, name):
        val = self.namespace.get(name)
        if val is not None:
            return val
        elif self.parent is not None:
            return self.parent.resolve(name)
        else:
            raise errors.PicNameError('Name {!s} not defined'.format(name))
