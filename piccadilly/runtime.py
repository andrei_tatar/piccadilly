#!/usr/bin/env python

# Copyright (c) 2014 Andrei Tatar. All rights reserved.

import types
import threading

from piccadilly.lang import Block, Identifier
from piccadilly.errors import PicNameError, PicRuntimeError


def string_cast(element):
    if element.startswith('"') and element.endswith('"'):
        return element[1:-1]
    else:
        raise ValueError('Invalid string literal')


class Interpreter(object):

    def __init__(self, block, stackseed=None):
        if stackseed is not None:
            self.stack = stackseed
        else:
            self.stack = []
        self.block = block
        self.bpflag = threading.Event()
        self.casts = (
            string_cast,
            int,
            float
        )

    def step(self, element):
        if not (isinstance(element, Block) or isinstance(element, basestring)):
            raise PicRuntimeError('Invalid statement: {!s}'.format(element))

        if isinstance(element, Block):
            self.stack.append(element)
        elif element.startswith('/'):
            self.stack.append(Identifier(element[1:]))
        elif element.startswith('&'):
            obj = self.block.resolve(Identifier(element[1:]))
            if isinstance(obj, Block):
                self.stack.append(obj)
            else:
                raise PicRuntimeError('Invalid dereferencing for {!s}'.format(element[1:]))
        else:
            for cast in self.casts:
                try:
                    self.stack.append(cast(element))
                except ValueError:
                    continue
                break
            else:
                obj = self.block.resolve(Identifier(element))
                if isinstance(obj, types.FunctionType):
                    obj(self.stack, self.block.namespace)
                elif isinstance(obj, Block):
                    obj.namespace.clear()
                    Interpreter(obj, stackseed=self.stack).run()
                else:
                    self.stack.append(obj)

    def run(self, *breakpoints):
        for index, element in enumerate(self.block):
            if index in breakpoints:
                self.bpflag.wait()
                self.bpflag.clear()
            self.step(element)

    def resume(self):
        self.bpflag.set()
