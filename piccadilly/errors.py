#!/usr/bin/env python

# Copyright (c) 2014 Andrei Tatar. All rights reserved.


class PicError(Exception):
    pass


class PicSyntaxError(PicError):
    pass


class PicRuntimeError(PicError):
    pass


class PicNameError(PicRuntimeError):
    pass
