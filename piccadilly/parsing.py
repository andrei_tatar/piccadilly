#!/usr/bin/env python

# Copyright (c) 2014 Andrei Tatar. All rights reserved.

import re
import inspect

from piccadilly import lang
from piccadilly import errors
from piccadilly import builtins

def parse(source):
    source, commentno = re.subn('#.*', '', source)
    tokens = re.findall('[^\s"]+|"[^"]*"', source.replace('\n', ' '))
    block = lang.Block()
    for token in tokens:
        if token == '{':
            block = lang.Block(parent=block)
        elif token == '}':
            block.parent.append(block)
            block = block.parent
        else:
            block.append(token)
        if block is None:
            raise errors.PicSyntaxError('Invalid block closure')
    if block.parent is not None:
        raise errors.PicSyntaxError('Hanging open block')
    funcs = [f for f in inspect.getmembers(builtins, inspect.isfunction) if f[0].startswith('_')]
    block.inject_names({lang.Identifier(f[0][1:]) : f[1] for f in funcs})
    return block
