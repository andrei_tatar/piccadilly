#!/usr/bin/env python

# Copyright (c) 2014 Andrei Tatar. All rights reserved.

from __future__ import division

import inspect

from piccadilly import errors
from piccadilly import lang
from piccadilly import runtime


def stackop(func):
    count = len(inspect.getargspec(func).args)

    def repl_func(stack, *args):
        pops = pop(stack, count)
        ret = func(*pops)
        if ret is not None:
            if isinstance(ret, tuple):
                stack.extend(ret)
            else:
                stack.append(ret)

    return repl_func


def pop(stack, no):
    try:
        return reversed([stack.pop() for i in xrange(no)])
    except IndexError:
        raise errors.PicRuntimeError('Pop from empty stack')

### Stack utilities

def _sp(stack, *args):
    stack.append(len(stack))

def _clear(stack, *args):
    del stack[:]

@stackop
def _exc(a, b):
    return b, a

@stackop
def _dup(a):
    return a, a

@stackop
def _ndup(val, no):
    return tuple((val for i in xrange(no)))

@stackop
def _pop(a):
    pass

def _npop(stack, *args):
    n, = pop(stack, 1)
    pop(stack, int(n))

### Basic I/O

@stackop
def _print(what):
    print what,

@stackop
def _println(what):
    print what

@stackop
def _in():
    return raw_input()

### Logic operators

@stackop
def _not(a):
    return not a

@stackop
def _and(a, b):
    return a and b

@stackop
def _or(a, b):
    return a or b

### Bitwise operators

@stackop
def _band(a, b):
    return a & b

@stackop
def _bor(a, b):
    return a | b

@stackop
def _bxor(a, b):
    return a ^ b

### Comparisons

@stackop
def _eq(a, b):
    return a == b

@stackop
def _lt(a, b):
    return a < b

@stackop
def _gt(a, b):
    return a > b

### Language basics

def _def(stack, namespace):
    val, name = pop(stack, 2)
    namespace[name] = val

def _if(stack, *args):
    cond, trueblock, elseblock = pop(stack, 3)
    msg = 'Invalid execution block for "if" statement: {!s}'
    if not isinstance(trueblock, lang.Block):
        raise errors.PicRuntimeError(msg.format(trueblock))
    if not isinstance(elseblock, lang.Block):
        raise errors.PicRuntimeError(msg.format(elseblock))
    if cond == 1:
        runtime.Interpreter(trueblock, stack).run()
    else:
        runtime.Interpreter(elseblock, stack).run()

def _while(stack, *args):
    condblock, codeblock = pop(stack, 2)
    msg = 'Invalid {type} block for "while" statement: {block!s}'
    if not isinstance(condblock, lang.Block):
        raise errors.PicRuntimeError(msg.format(type='condition', block=condblock))
    if not isinstance(codeblock, lang.Block):
        raise errors.PicRuntimeError(msg.format(type='execution', block=codeblock))
    while True:
        runtime.Interpreter(condblock, stack).run()
        cond, = pop(stack, 1)
        if cond:
            runtime.Interpreter(codeblock, stack).run()
        else:
            break

def _for(stack, *args):
    start, inc, finish, block = pop(stack, 4)
    start, inc, finish = int(start), int(inc), int(finish)
    if not isinstance(block, lang.Block):
        raise errors.PicRuntimeError('Invalid execution block for "for" statement: {!s}'.format(block))
    for i in xrange(start, finish, inc):
        stack.append(i)
        runtime.Interpreter(block, stack).run()

def _do(stack, *args):
    block, = pop(stack, 1)
    if not isinstance(block, lang.Block):
        raise errors.PicRuntimeError('Invalid execution block for "do" statement: {!s}'.format(block))
    runtime.Interpreter(block, stack).run()

### Math basics

@stackop
def _add(a, b):
    return a + b

@stackop
def _neg(a):
    return -a

@stackop
def _mul(a, b):
    return a * b

@stackop
def _div(a, b):
    return a // b

@stackop
def _mod(a, b):
    return a % b

@stackop
def _fdiv(a, b):
    return a / b

### Casts

@stackop
def _int(a):
    return int(a)

@stackop
def _float(a):
    return float(a)

@stackop
def _str(a):
    return str(a)

@stackop
def _bool(a):
    return bool(a)
